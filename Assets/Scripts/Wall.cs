﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {
    public PongGame _game;

    public bool ScoreForPlayer1 = true;

    void Awake() {
        if(_game == null) {
            _game = GameObject.FindObjectOfType<PongGame>();
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(ScoreForPlayer1 == true) {
            _game.Player1Score();
        }
        else {
            _game.Player2Score();
        }
    }
}
