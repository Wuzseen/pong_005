﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PongGame : MonoBehaviour {
    public int scoreLimit = 5;
    private int player1Score = 0;
    private int player2Score = 0;

    public Ball _ball;

    public Text scoreText;


    void SetScoreText() {
        scoreText.text = player1Score.ToString() + " : " + player2Score.ToString();
    }

	// Use this for initialization
	void Start () {
        SetScoreText();
	}

    public void Player1Score() {
        player1Score++;
        SetScoreText();
        _ball.Reset();
    }

    public void Player2Score() {
        player2Score++;
        SetScoreText();
        _ball.Reset();
    }
}
