﻿using UnityEngine;
using System.Collections;

public class PlayerPaddle : MonoBehaviour {
    public string InputAxis = "Vertical";

    public float moveSpeed = 5f;

    private Rigidbody2D rgdbody;

	// Use this for initialization
	void Start () {
        this.rgdbody = this.GetComponentInChildren<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float verticalInput = Input.GetAxisRaw(InputAxis);
        Vector3 offset = Vector3.zero;
        offset.y = verticalInput * moveSpeed * Time.deltaTime;
        rgdbody.MovePosition(transform.position + offset);
	}
}
