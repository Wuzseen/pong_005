﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
    public float startSpeed = 5f;

    private Rigidbody2D rgdbody;

    private Vector3 startPosition;

    // Use this for initialization
    void Start () {
        startPosition = this.transform.position;
        this.rgdbody = this.GetComponentInChildren<Rigidbody2D>();
        Reset();
    }

    public void Reset() {
        this.transform.position = startPosition;
        float leftRight = 1f;
        if(Random.Range(0f,1f) > .5f) {
            leftRight = -1f;
        }
        this.rgdbody.velocity = Vector3.right * startSpeed * leftRight;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("paddle")) {
            Vector3 vel = this.rgdbody.velocity;
            vel.x *= -1.1f;
            float dist = this.transform.position.y - other.transform.position.y;
            vel.y = dist;
            this.rgdbody.velocity = vel;
        }
        else if(other.CompareTag("wall")) {
            print(rgdbody.velocity);
            Vector3 vel = this.rgdbody.velocity;
            vel.y *= -1f;
            this.rgdbody.velocity = vel;
        }
    }
}
