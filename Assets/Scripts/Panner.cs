﻿using UnityEngine;
using System.Collections;

public class Panner : MonoBehaviour {
    public Material _mat;
    public float xSpeed, ySpeed;
	// Use this for initialization
	void Start () {
        this._mat = this.GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        _mat.mainTextureOffset += new Vector2(xSpeed * Time.deltaTime, ySpeed * Time.deltaTime);
	}
}
